FROM golang:1.22 as builder

RUN go install -ldflags="-extldflags=-static" -tags sqlite github.com/davecheney/pub@latest
RUN mkdir /data


FROM scratch as runtime

COPY --from=builder /go/bin/pub /bin/pub
COPY --from=builder --chown=1000:1000 /data /data

USER 1000:1000
VOLUME /data
WORKDIR /data
ENTRYPOINT ["/bin/pub"]
