# Docker image of pub "a tiny ActivityPub to Mastodon bridge"

## What is pub?
Go to its repository: https://github.com/davecheney/pub

## LICENCE
BSD 3-clause

## Credits
All credits go to Dave Cheney.
